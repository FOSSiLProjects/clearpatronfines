;Clear Patron Fines
;Rewrite - November 27, 2018


; First, let's make sure things move as quickly as possible.
SendMode Input

; Set up the environment.
#SingleInstance Force
SetWorkingDir %A_ScriptDir% ;Home is where the script is.
csvDataFile := "PatronWaives.csv" ;Primary input file.
csvExcept := "Exceptions.csv" ;File used to gather exceptions.

; Let's get some kinda idea how long this will take
Loop, Read, %csvDataFile%
{
   total_lines = %A_Index%
}

; Build the GUI monitoring.

lines_remain := total_lines

Gui, +AlwaysOnTop
Gui, Font, s14 bold, Arial
Gui, Add, Text,, Waives in progress
Gui, Font, s10 norm, Arial
Gui, Add, Text,, Total Lines: %total_lines%
Gui, Add, Text,vRem, Lines Remaining: %lines_remain%
Gui, Add, Text,vCurrent, Current record: xxxxxxxx
Gui, Add, Text,vPrevious, Previous record: xxxxxxxx
Gui, Add, Text,vPerc, Percentage Complete: 0.0000
Gui, Add, Button, gKillApp, &Stop ;Give you the ability to manually kill the script.
Gui, Show, x1 y1 h250 w310, Patron Waive Progress
Sleep, 2000

; Parse the data in the file.
Loop, Read, %csvDataFile%

{
	Loop, Parse, A_LoopReadLine, CSV
 {
	; Assign variables to data.
	if (A_Index = 1)
		recordID := A_LoopField
	}
	
	; Begin sending the data to the Polaris ILS Staff Client.
	; Make sure the Patron Status Find Tool is open and cleared.
	Check := 0 ; Set a switch to see if an exception was thrown.
	GuiControl,,Current, Current record %recordID%
	SetTitleMatchMode, 2
	WinActivate, Patron Records - Record ID Find Tool
	Send !n
	Send %recordID%
	Sleep, 500
	Send {ENTER}
	Sleep, 1500
	Send {ENTER}
	WinWaitActive, Patron Status
	Send !va
	SetTitleMatchMode RegEx
	WinWaitActive, Patron Status .* Account .*
	Send ^v
	WinWaitActive, Waive
	Send {TAB}{TAB}{TAB}
	Send {TAB}
	Send SCRIPTED MESSAGE: Inactive account - Clearing fines before purging. DLM @ AD
	Send {ENTER}
	WinWaitActive, Patron Status .* Account .*
	Send !{F4}
	WinWaitActive, Patron Records - Record ID Find Tool
	Send !n
	
	; Update GUI monitoring.
	
	;Update the maths.
	lines_remain -= 1
	update_percent := Round(((total_lines - lines_remain) / total_lines) * 100)
	
	; Update the GUI
	previous := recordID
	GuiControl,,Previous, Previous record: %previous%
	GuiControl,,Rem, Lines Remaining: %lines_remain%
	GuiControl,,Perc, Percentage Complete: %update_percent%
	
	If lines_remain = 0
	{
		MsgBox,0,Complete,Waives complete.`nAll accounts processed.
		ExitApp
	}
		
}

KillApp:
{
	MsgBox,0,Script Terminated,Current record: %recordID%`nPrevious record: %previous%
	ExitApp
}