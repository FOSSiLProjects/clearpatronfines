SendMode Input ; Speeding up the input strings

; In which we define files

; The data file used for input - use CSV
csvDataFile := "C:\Temp\PatronWaives.csv" 
; The exceptions file that will be generated if there is a problem
csvExcept := "C:\Temp\Exceptions.csv"

; Let's get some kinda idea how long this will take
Loop, Read, %csvDataFile%
{
   total_lines = %A_Index%
}

; In which we build a GUI

lines_remain := total_lines
;percent_complete := (total_lines - lines_remain) / ((lines_remain + total_lines) / 2) * 100

Gui, +AlwaysOnTop
Gui, Font, s14 bold, Arial
Gui, Add, Text,, Waives in progress
Gui, Font, s10 norm, Arial
Gui, Add, Text,, Total Lines: %total_lines%
Gui, Add, Text,vRem, Lines Remaining: %lines_remain%
Gui, Add, Text,vCurrent, Current record: xxxxxxxx
Gui, Add, Text,vPrevious, Previous record: xxxxxxxx
Gui, Add, Text,vPerc, Percentage Complete: 0.0000
Gui, Add, Button, gKillApp, &Stop
Gui, Show, x1 y1 h250 w310, Patron Waive Progress
Sleep, 2000

; Parse the data in the file
Loop, Read, %csvDataFile%

{
	Loop, Parse, A_LoopReadLine, CSV
 {
	; Assign variables to data on both sides of the comma
	if (A_Index = 1)
		barcode := A_LoopField
	}
			
	; Begin sending the data to Polaris ILS - Staff Client. Make sure the Patron Status Find Tool is open and cleared.
	Check := 0 ; Set a switch to check if an exception was handled.
	GuiControl,,Current, Current record %barcode%
	SetTitleMatchMode, 2
	WinActivate, Patron Records - Record ID Find Tool
	Send !n
	Sleep, 1000
	Send %barcode%
	Sleep, 500
	Send {ENTER}
	Sleep, 4000
	Send {ENTER}
	Sleep, 4000
	WinActivate, Patron Status
	Send !va
	Sleep, 2000
	Send ^v
	Sleep, 2000
	Gosub, Exception ; Check to see if an exception has been thrown.
	If Check = 0 ; If no exception has been thrown, then Check remains at 0. Otherwise it's set to 5.
	{
		WinActivate, Waive
		Send {TAB}{TAB}{TAB}
		Send {TAB}
		Send SCRIPTED MESSAGE: Inactive account - Clearing fines before purging. DLM @ AD
		Send {ENTER}
		sleep, 4000
		Gosub, Exception2
		If Check = 0
		{
		WinActivate, Patron Status
		Send !{F4}
		Sleep, 2000
		WinActivate, Patron Records - Record ID Find Tool
		Send !n
	}
	}
	If Check = 5
	{
		continue
	}
	
	; Update the maths
	lines_remain -= 1
	update_percent := Round(((total_lines - lines_remain) / total_lines) * 100)
	;sleep, 500
	
	; Update the GUI
	previous := barcode
	GuiControl,,Previous, Previous record: %previous%
	GuiControl,,Rem, Lines Remaining: %lines_remain%
	GuiControl,,Perc, Percentage Complete: %update_percent%
	

	; Check to see if we're done yet
	If lines_remain = 0
		;break
		ExitApp
}


Exception:
;Check for problems with waiving the fine
	SetTitleMatchMode, 3
	IfWinNotExist Polaris
		return
	else
	{
		FileAppend, %barcode% %fee%`n, %csvExcept%
		Sleep, 4000
		WinActivate, Polaris
		Send {ENTER}
		Sleep, 4000
		SetTitleMatchmode, 2
		WinActivate, Patron Status
		Sleep, 4000
		Send !{F4}
		Sleep, 4000
		WinActivate, Patron Records - Record ID Find Tool
		Sleep, 4000
		SEND !n
		barcode :=
		fee :=
		Check := 5
		return
	}
	
	Exception2:
	;Check for problems with too much money waived
	SetTitleMatchMode, 3
	IFWinNotExist Polaris
		return
	else
	{
		FileAppend, %barcode% %fee%`n, %csvExcept%
		Sleep, 4000
		WinActivate, Polaris
		Send {ENTER}
		Sleep, 4000
		Send {TAB}{TAB}{TAB}{TAB}
		Send {SPACE}
		SetTitleMatchmode, 2
		WinActivate, Patron Status
		Sleep, 4000
		Send !{F4}
		Sleep, 4000
		WinActivate, Patron Records - Record ID Find Tool
		Sleep, 4000
		SEND !n
		barcode :=
		fee :=
		Check := 5
		return
	}
	
KillApp:
{
	MsgBox,0,Script Terminated,Current record: %barcode%`nPrevious record: %previous%
	ExitApp
}

	
; Import completed
Gui, Destroy
MsgBox,, Waives complete.
ExitApp