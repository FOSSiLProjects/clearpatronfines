# Clear Patron Fines

#### Requires:

* AutoHotkey
* Windows (7, 8, 10, 11)
* Polaris ILS - Supported: 4.1R2 - 7.3

---

![clearpatronfines](https://i.imgur.com/1FT0zsH.png)


#### Latest Changes (2023/10/01)

* Small updates to work on a remote Windows Server with Polaris 7.3
* Deployed on Inland Library Network for Moorpark, CA fine free transition
* Cleaned up the repo and stuffed the old version into an archive folder

#### Latest Changes (2019/04/12)

* The code is able to wait for the Polaris Find Tool to complete a search before continuing. You may need to change the window information in line 59. Use Window Spy to get the word Done in the status bar of the Polaris Find Tool. (Contact cyberpunklibrarian at protonmail dot com if you have questions.)
  * The makes the code *much* more reliable, almost eliminating restarts due to timing issues with the Find Tool

#### Latest Changes (2018/11/29)

* Complete rewrite of the code
* All timer based tasks updated to event based tasks
* Four times faster than the previous versions
  * Able to clear approximately 400 accounts in a half-hour period (depending on server and connection speeds)

#### Latest Changes (2018/11/20)

* Basic record tracking on the GUI - Current and previous records displayed
* Hitting the stop button throws a message box with the last two records handled
* Slight workflow adjustments

#### Recent Changes (2018/11/13)

* Added a Stop button to kill the script right from the GUI
* The script will gracefully exit after all changes have been made
* Relocated the position of the GUI to the top left corner
* GUI is set to "always on top"

Clear Patron Fines is a script written in AutoHotkey that uses a CSV file to waive fines on a patron account in Polaris ILS. When faced with a patron purge, fine forgiveness event (such as a food for fines programme), or other job that requires fines to be waived on multiple patron accounts; there's no good way to do this in the  Polaris Staff Client or through the database using SQL. To mitigate this, I wrote a script that takes a list of patron barcodes from a CSV and feeds them to the Polaris patron status workform, waiving fines through keyboard commands.

### To Use

Clear Patron Fines is set to work from the C:\Temp directory, but this can be altered as you like. A CSV of Patron IDs obtained through SimplyReports or SQL provides the input for the script. To set up, open the Patron Status Find Tool in the Polaris Staff Client, and set it to search by Record ID. Save that as the default so it doesn't revert while the script runs. Clear the search by clicking New Search and then double-click the script to launch. 

I recommend standing by while the script runs. There's sufficient timing in the script to allow for lag and database slowness, but I always keep an eye on it while it works. Just in case.

### Questions?

cyberpunklibrarian (at) protonmail (dot) com
